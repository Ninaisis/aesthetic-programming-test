let witch;
//let min_sign = 5;
//let sign = [];
let witchgirl;
let sailormars;
let poison;
let bg;
let mySound;
let apple = [];
let data;
let lyrics;
let mybutton;

function preload() {
soundFormats('mp3');
mySound = loadSound('Purge_The_Poison.mp3');
data = loadJSON("lyrics.json");
witchgirl = loadImage('witchgirlmarina.gif');
poison = loadImage('poisonapple.png');
bg = loadImage('backgroundforpurgingpoison.png');

}

function setup() {
  amplitude = new p5.Amplitude();
    amplitude.setInput(mySound);
  createCanvas(1450, 700);
  witch = new Witch();
lyrics = data.lyrics;
len = mySound.duration;
mybutton = createButton('press for:   💗Purge the poison by Ma‍rina💗');
mybutton.position(1100, 320);
mybutton.mousePressed(mousePressed);
mybutton.style("background-color", "#FF0091");
mybutton.size(180,50);
  }
function keyPressed() {
  if (key == ' ') {
    witch.jump();


  }
}
function draw() {
if (random(3) < 0.03) {
apple.push(new Poison());
}

background(bg);
fill(0);
textSize(12);
text('Press the spacebar to avoid the posioned apples\n                       if you want :)', 30, 480);

witch.show();
witch.move();

let verse1 = lyrics[0].members;
let verse2 = lyrics[1].members;
let verse3 = lyrics[2].members;
let verse4 = lyrics[3].members;
let verse5 = lyrics[4].members;
let verse6 = lyrics[5].members;
let verse7 = lyrics[6].members;
let verse8 = lyrics[7].members;
let verse9 = lyrics[8].members;
let verse10 = lyrics[9].members;
let verse11 = lyrics[10].members;
let verse12 = lyrics[11].members;
let verse13 = lyrics[12].members;

fill (0);

textSize(16);
textStyle(BOLD);
textFont('Cursive');
text(verse1, 20, 20) ;
text(verse2, 100, 180);
text(verse3, 360, 20);
text(verse6, 580, 170);
text(verse7, 680, 20);
text(verse10, 1020, 180);
text(verse11, 1150, 20);
fill(255,239,16);
textSize(20);
//chorus
text(verse4, 460, 310);
text(verse5, 420, 430);

for (let a of apple) {
  a.move();
  a.show();
}

}

function mousePressed()
{
  // trigger sound
  mySound.play();

}
