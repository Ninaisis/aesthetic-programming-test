[*purge the poison*](https://ninaisis.gitlab.io/aesthetic-programming-test/minix7)

<img src="minix7/Screenshot_2021-04-24_at_23.39.21.png" width="900">

*Describe how does/do your game/game objects work?*

So for this miniX we had to work with objects, and the notion of object orientation. To do this i created to objects, or classes, and merged them into my program which functions as a type of "game" though this could be disputed. The program is a type of visualisation of the song which plays in the background, you could say I was inspired by the lyrics of the song which are also typed out and shown within the program. The user of the program is able to press the space button to make a with girl jump, while poisoned apples a rolling across the screen, though if the apples are touched nothing happens, which could change the idea of this being a "game" as there is nothing to gain or lose, you are simply afforded the choice to jump. For this miniX I also had my own objectives one being using JSON to load in the lyrics, and also to try to add sound to my program as these were things I hadn't tried, or been able to get to work previously. I am really into pixel art and this new wave of using "older/ more simple" graphics/mechanics when creating games even though technology has evolved beyond this, which is why all my objects and background consist og this. it is very nostalgic in a way. It was also my first time uploading a background into my sketch. 
 



*Describe how you program the objects and their related attributes, and the methods in your game.*

I got a lot of help from a Daniel Shiffmann video when coding the objects. I know from the start that i wanted to create an object that was going to my witch that could jump and an object that signified "poison" and here i liked the art of the apples. My main goal was to make one object move on to the screen continously and one object that had the ability to evade the moving object. I made the Witch able to jump but had to change a bunch of things within the class, as I wanted to make sure jumping felt fluid, so considering the size of this class, the gravity within the program, and the height of the jump had to be considered so that it would be possible to jump over the apples no matter how many were on screen. When creating the apple class I had to be aware of the speed and the amount of apples on screen. 

I also used both the move and show functions within each class - I also used these to call the objects into my main sketch. 


*Draw upon the assigned reading, what are the characteristics of object-orientedprogramming and the wider implications of abstraction?*

what happens when we put everything in boxes, . which things are left out. it is taken from our on worldview. you pick and choose 

OOP is more than just code, it is about the interplay between the abstract and the concrete or how the objects in code have a relation to the real world, therefore it is also a good opportunity to reflect about the world we live in when working with OOP.

*Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?*

"With the abstraction of relations, there is an interplay between abstract and concrete reality. The classifications abstract and concrete were taken up by Marx in his critique of capitalist working conditions to distinguish between abstract labor and living labor. Whereas abstract labor is labor-power exerted producing commodities that uphold capitalism, living labor is the capacity to work. (Cox, Soon, Aesthetic Programming, p. 161).

"Object abstraction in computing is about representation. Certain attributes and relations are
abstracted from the real world, whilst simultaneously leaving details and contexts out." - Aesthetic Programming (Object abstraction) by Winnie Soon and Geoff Cox, p. 146


refrences:
Daniel Shiffmann the Coding train:
https://www.youtube.com/watch?v=SfA5CghXw18 
https://www.youtube.com/watch?v=l0HoJHc-63Q&t=6s

pictures + gifs 
https://violetvenus.carrd.co/assets/images/image01.gif?v75860592147751
https://wallpaperaccess.com/full/2789327.jpg
https://lh3.googleusercontent.com/proxy/80Hpi7EqrfWz6-_DfsuEotnjcfgwRvfTc2CsZ-RUwJsCL0mKYjqvyPKVPIQ4Ja-5TRZ0Ys7hUJET2kRLRSh8YxCiBb1HAYnQ

song 
Purge the Poison by Marina 

Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). 

http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf
