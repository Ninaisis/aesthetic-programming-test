// biased.Gif.Poetry(); by Nina Isis & Sofie F. Mønster \\ <3

//global variables
var api = "https://api.giphy.com/v1/gifs/search?";
var apiKey = "&api_key=dc6zaTOxFJmzC";
//Systems critical blood fear fake values//

//buttons
var systemsb;
var criticalb;
var bloodb;
var fearb;
var fakeb;
var valuesb;
//:)
var flowerframe;
//preloading our flower gif :)
function preload() {
gif_loadImg = loadImage("6stc.gif");
gif_createImg = createImg("6stc.gif");
gif_createImg.position(990,20);
flowerframe = loadImage('flowerframe.png');
}

function setup() {
  //noCanvas(); //I use the DOM instead of canvas
frameRate(0.3)//did this to keep the flower animation
createCanvas(1430, 300);
  background(255,0,0,0);
//our text box
  fill(252,182,114,30);
  stroke('pink');
rect(370, 5, 700, 180);
//calling our flowerframe
image(flowerframe, 290, -20, 400, 280);
//our poem written by Mark Tey
  fill(90,70,164);
  textFont("Helvetica");
  textSize(20);
  textAlign(CENTER);
  text(' You rely on systems as you forgot to think for yourself; \n Critical thinking was left on a shelf. \n And as you dwell, in a flood of news, you see the blood of fews; \n To make you fear or leave you feeling blues; \n Be them true or fake, you believe in the message they want to create; \n So fake is the new awake, to tear you apart from the values of your heart', 715, 30);
//our lil buttons :)
  createMenuButtons();
}
//styling all our buttons :D

function createMenuButtons() {

  systemsb = createButton('systems');
  systemsb.position(575, 14);
  systemsb.mousePressed(systems);
  systemsb.style("color", "#6600CC");
  systemsb.size(110,23);
//systemsb.style("background-color", "#C09FE5"); ? do we want this color?

  criticalb = createButton('critical');
  criticalb.position(530, 39);
  criticalb.mousePressed(critical);
  criticalb.style("color", "#6600CC");
  criticalb.size(97,22);

  bloodb = createButton('blood');
  bloodb.position(869, 63);
  bloodb.mousePressed(blood);
  bloodb.style("color", "#6600CC");
  bloodb.size(56,22);

  fearb = createButton('fear');
  fearb.position(633, 88);
  fearb.mousePressed(fear);
  fearb.style("color", "#6600CC");
  fearb.size(42,22);

  fakeb = createButton('fake');
  fakeb.position(547, 112);
  fakeb.mousePressed(fake);
  fakeb.style("color", "#6600CC");
  fakeb.size(43,22);

  valuesb = createButton('values');
  valuesb.position(860, 138);
  valuesb.mousePressed(values);
  valuesb.style("color", "#6600CC");
  valuesb.size(67,22);


}

function gotData(giphy) { //data from api

removeElements(); //we want the old gifs to disappear when a new button is pressed

createMenuButtons();

  for (var i = 0; i < 30; i++) { //for loop to make the different GIFS appear on canvas

    createImg(giphy.data[i].images.original.url);
  }

}
// using that api hihihi
function getQueryData(query){
  var url = api + apiKey + query;
  giphy = loadJSON(url, gotData);

}
//all our functions mainly made for the different query data :)
function systems() {
  if (systemsb.mousePressed) {
    getQueryData("&q=system+data+technology+algorithm+code+computer+structures+powerstructures+capitalism+patriarchy");

  }
}
function critical() {
  if (criticalb.mousePressed) {
    getQueryData("&q=+critical+thinking+codedbias+climatechange+globalwarming+reform+abolish");


  }
}
function blood() {
  if (bloodb.mousePressed) {
    getQueryData("&q=breonnataylor+icantbreath+femicide+menstruation+meatindustry+policeviolence");


  }
}
function fear() {
  if (fearb.mousePressed) {
    getQueryData("&q=anxiety+afterdark+metoo+rape+murder+femicide+globalwarming+hatecrime+textmewhenyougethome");


  }
}
function fake() {
  if (fakeb.mousePressed) {
    getQueryData("&q=fakenews+qanon+fakehappy+instagram+kardashian+slacktivism+sustainable+consumption+under+capitalism");


  }
}
function values() {
  if (valuesb.mousePressed) {
    getQueryData("&q=solidarity+feminism+queerspaces+lgbtqi+opensource+criticalthinking+translivesmatter+blm+animalrights");


  }
}
//calling our flower<3
function draw() {

  gif_createImg = createImg("6stc.gif");
  gif_createImg.position(990,20);

}
// we hope you enjoyed looking at our program, have a nice day xoxo <3 \\
