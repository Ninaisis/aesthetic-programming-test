[*generative femicide*](https://ninaisis.gitlab.io/aesthetic-programming-test/minix5)

<img src="minix5/Screenshot_2021-03-14_at_21.54.30.png" width="800">

**What is the program about?**

So for this week we were told to create a generative program that had random elements but still works within a set of structures set in place by me. And As I like combining this type of computational work with real world problems. the first thing i thought about was 'femicide', coincidentally this theme is very talked about right now, and even though I had already thought of this being my theme weeks ago, the recent news definitely played a role in the making of this program. You might be wondering why I thought about this theme, and how a generative program and femicide overlap. though it can be seen as somewhat of a random occurrence when women are killed (most often) by a man, partner or ex-partner, it is clear when you look at statistics that this is a serious issue and that there are obviously structures in place that are enabling this. 

I wanted my program to have a feminine feel to it, and the first thing that popped into my mind were roses, I wanted a rose that was alive and full of life and also one that was black and white/ lacking colour to signify the women who are killed. 

The gray rose generates randomly, though it will keep generating. I wish i could've put in the proper maths to underline the statistics, but i just played around with the random(); until i felt the rate in which the gray rose generated somewhat fit the statistics. I also added a little message up in the left corner that says "text me when you get home xx" which is a thing I know most women have felt the need to text or have received when going home after dark. I also added a type of grid that gets darker and darker to signify night time, and it is also why it is darkest in the top left corner where the text message is. There are also a couple of rect() with little pieces of information pertaining to the theme and statistics I found to be relevant. It's really important for me to also enlighten people on some of the issues I find to be important, and I know that this is something many people have no idea about, and are often shocked when they hear it. I also like that the red rose keeps generating on top of the gray one, and also on top of the little info boxes, just like how these stories often get buried og brushed away as a (random) one time occurence, and not acknowledged as a serious structural issue that in turn makes every woman afraid, and even more so women of color, or non cis-women. 
 
**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**

So in my program I have a conditional statement (if{} else{}) that in turn makes the red rose generate and then though this is random() if (random(3) < 0.5 &&  random(1) < 0.9) { it creates a gray rose. over time it will just keep generating the red rose until it hits x > 1400 then it will go back and start again. The background will also get darker slowly from left to right. 

**What role do rules and processes have in your work?**

It helps express the message I want to deliver. Again though it is a random occurence that a gray rose is generated I have set structures in place that no matter what will keep generating gray roses. just like how the killing of women may be seen as a random occurence but if one looks at the statistics it shows that there must be structures set in place to enable this. 

**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**

I mean I really like this idea of 'collaborating' with the computer and using each others strengths to create something meaningful. But I also find it to be a little scary when things are auto generated with no oversight - what type of damage occurs when we don't look into things and assume when something is random it is also 'neutral' 'un-biased' (just like when we don't look behind what structures are in place that makes it possible for women to be enduring so much violence at the hands of men without any structural changes happening )

 I find the making of functions and setting rules to be really confusing for me. So I want to work with it more. 

  xoxo Nina :)

  **references**

- http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf
- https://en.wikipedia.org/wiki/Femicide
- https://www.information.dk/indland/2019/08/ny-forskning-300-536-kvindedrab-begaaet-partneren
- https://powerkvinderne.dk/liste/942-maend-der-draeber-kvinder-folger-et-helt-bestemt-monster
- https://www.theguardian.com/society/2020/nov/22/if-im-not-in-on-friday-i-might-be-dead-chilling-facts-about-uk-femicide
- https://10print.org/10_PRINT_121114.pdf



