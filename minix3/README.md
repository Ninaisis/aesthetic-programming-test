[T𝔥𝔢 𝔣𝔢𝔪𝔦𝔫𝔦𝔫𝔢 𝔡𝔦𝔪𝔢𝔫𝔰𝔦𝔬𝔫𝔰 𝔬𝔣 𝔱𝔦𝔪𝔢](https://ninaisis.gitlab.io/aesthetic-programming-test/minix3/)

<img src="minix3/Screenshot_2021-02-21_at_19.59.01.png" width="800">


**What do you want to explore and/or express?**

So for this minix I started out knowing only that I wanted to make something with the moon, and it's connection to the feminine. This is also where the aspect of time fit quite well because I have recently been reading about how 'women' (also nb humans and anyone with hormones that fluctuate on a monthly basis) are forced to live as if they are the same from day to day though a woman and her body go through a whole cycle of hormonal changes throughout a 29 day period, this no matter if they have periods or not. This opposed to mens 24 hours hormonal cycle that our society is built upon. I like the idea of time being more of a cycle than a straight forward line that just goes on forever. So this is why there are the 12 moons that go from crescent to full and a moon png in the middle, i chose 12 moons and a dial going around slowly to mimic a watch so it in this way also refers to some sort of construction of time. I added the text because I liked the idea of people having something to read while they wait in all eternity as my throbber-page never ends! and also felt it was important to understand the concept even without reading the read me.. and also I wanted to learn how to get text to jump lines hehe :)

**What are the time-related syntaxes/functions that you have used in your
program, and why have you used them in this way? How is time being
constructed in computation (refer to both the reading materials and
your coding)?**

The first thing I did was try to find some code that was showing the cycle of the moon, i found this by simply typing in 'moon cycle p5 js' it is very easy to find inspiration for something specific like this! I then copied part of this code, as the moon cycle is obvously the most advanced piece of code within my program I thought it was important to point out that even now there are still parts of it I don't understand those parts are also clearly pointed out with the // text. Though I must say I also learnt a lot from doing this and it was actually pretty fun to play around with, and to make it fit my idea, and my program. Other than this I also had some personal goals in terms of specific syntaxes I wanted to learn how to use. These were things like using variables (let x = x), and making my own, uploading images(via Let x and loadImage()), using an if statement (if (x) { do this }), and getting text to sit on different lines (syntax is textLeading()). These were all things I had wanted to do in earlier programs but couldn't get to work! So I feel like I learnt many many things this week and actually had a lot of fun making minix3. Some of the other syntaxes I used such as the rotation(rotate(radians(cir)) paired with let cir = 360/num*(frameCount%num). I had used these on previous minix so for me this wasn't too new though it gets easier and easier to understand each new week! I also used both framerate and and the Let num = function to play around with speed and 'time' in my program! I also used the push and pop functions to control what was affected by the the roation sequence. There is also a For loop within the changing moon part of the code.

also want to add that I had started my program with createCanvas(windowWidth, windowHeight); and made all the shapes and sizes fit in the window i had opened, i then uploaded my sketch and quickly found out that as soon as the window size was changed just a little nothing fit! at first I was really scared that I wouldn't be able to fix this but i remembered we had learnt that with the print ('hello world'); function we could actually go into the console and write 'windowWidth & windowHeight and it would tell us the size of the canvas and I could then change my createCanvas to fit what I had originally worked with! phew..

: added from the feedback I got from Cathrine and Line<3 they found the perfect quote for me to add to this readme 

_”The experience of time is an essential element of any form of experience or cognition. Emotions depend to a large extent on expectations, or the potential coming of a future event. Any observing or experience of difference, of presence related to an earlier or later absence, is linked with an experience of time. […] Also, the experience of time is a conglomerate of different experiences: time as a common moment, the duration of a certain time, time as cyclic events, historical time, and so on”. (Lammerant 2018, p 89)_


**Think about a throbber that you have encounted in digital culture, e.g. for
streaming video on YouTube or loading the latest feeds on Facebook, or waiting
for a payment transaction, and consider what a throbber communicates, and/or
hides? How might we characterize this icon differently?**

so I swear I started this project with the intention of just making a throbber but I got a little carried away and it is more a throbber-page...But one throbber I remember very clearly from when I was young was the loading symbol for Animal Crossing Wild World a nintendo game I always played when I was younger. I remember it as being very hypnotising and almost like a symbol for time stopping, as i sat in anticipation for the game to start. It says to consider what a throbber communicates/hides and here the best thing I can describe it with is: remember how old computers would write a bunch of code on a screen before it was ready to be used, I see a throbber as what has come instead so you aren't shown all the stuff going on behind. It is like when you go to the doctor and you announce your arrival and then you are told to sit in the waiting room and wait on the doctor.. if that makes sense! I think this is all my brain can muster for today...

xoxo Nina (0___=)

references:

(the moon cycle part of the code is from this link)
- https://openprocessing.org/sketch/219297/

- http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf

- https://p5js.org/reference/

- https://www.google.com/search?q=moon+png&client=firefox-b-d&sxsrf=ALeKk00mjJuMF1FAf5RU4T629wvQOXW21Q:1620486355388&tbm=isch&source=iu&ictx=1&fir=7vvzoLGWPNCRHM%252Cov2LlV6ggDr28M%252C_&vet=1&usg=AI4_-kSb6WPh_KTdjAjdj_ELxqbgD3ijdg&sa=X&ved=2ahUKEwjaiM2MrrrwAhU8gf0HHRWgCg0Q9QF6BAgREAE&biw=1358&bih=657#imgrc=7vvzoLGWPNCRHM

- https://www.google.com/search?q=eart+png&tbm=isch&ved=2ahUKEwjJnNyOrrrwAhUY7rsIHUVuA4MQ2-cCegQIABAA&oq=eart+&gs_lcp=CgNpbWcQARgAMgQIABBDMgIIADICCAAyBAgAEEMyAggAMgIIADICCAAyAggAMgIIADICCAA6BAgjECdQ6PQBWOT5AWCgggJoAHAAeACAAXeIAdoDkgEDNC4xmAEAoAEBqgELZ3dzLXdpei1pbWfAAQE&sclient=img&ei=16iWYInVMZjc7_UPxdyNmAg&bih=657&biw=1358&client=firefox-b-d#imgrc=2o1IKlM5_8Q_WM

