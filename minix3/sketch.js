// my variables
let canvas, ctx, size;
let img;
let imgSizeX;
let imgSizeY;
let img1;
let img1sizeX
let img1sizeY

const total = 12; //how many moons
const duration = 4000; //speed of the shadow ellipse on the moon
easeInOut = t => 4*pow(t-0.5,3)+0.5;//also not quite sure about this hehe

function setup() {
  // put setup code here
  print('hello world');//needed this to figure out the width and height of the canvas
  createCanvas(1424, 700);
  frameRate(20);
  size = min(width, height) / 10;

	canvas = createGraphics(size, size); //not sure
	canvas.noStroke(); //not sure
	ctx = canvas.drawingContext; //not sure

  img = loadImage('moonPNG.png'); //my images and their sizes
  imgSizeX = (280);
  imgSizeY = (230);
  img1sizeX = (200);
  img1sizeY = (220);
  img1 = loadImage('earthPNG.png');
}

function draw() {
  // put drawing code here
  background(0);

  textSize(50);
  drawElements();
  for(let i = 0; i < total; i++){ //something on the timing of the shadow/moon
		let progress = i / total;
		let loop = easeInOut((millis()/duration + progress)%1);

		canvas.clear(); //all the shapes of the circle of moons
		ctx.globalCompositeOperation="source-over";
		canvas.fill(218, 219, 186);
		canvas.rect(0, 0, 300, 300);
		canvas.fill(0);
		canvas.ellipse(loop*size*2-size/2, size/2, size-0, size-0);
		ctx.globalCompositeOperation="destination-atop";
		canvas.ellipse(size/2, size/2, 65, 69);

		let angle = progress * TWO_PI; //the size of the cirle
		let x = cos(angle) * size * 4;
		let y = sin(angle) * size * 4;
		image(canvas, (width-size)/2 + x, (height-size)/2 + y);
}

function drawElements() {
//my dial
 let num=500; //how many frames it touches in one cycle
 push();
 translate(width/2, height/2);
 let cir = 360/num*(frameCount%num);
 rotate(radians(cir));
noStroke();
fill(230, 230, 215);
textSize(13);
 text('𝔱𝔥𝔢 𝔣𝔢𝔪𝔦𝔫𝔦𝔫𝔢 𝔡𝔦𝔪𝔢𝔫𝔰𝔦𝔬𝔫𝔰 𝔬𝔣 𝔱𝔦𝔪𝔢',width/12, height/30);
pop();
stroke(218, 219, 186);
line(1100,0,1100,height);
 stroke(218, 219, 186);
 line(320,0,320,height);
 image (img, 590, 240, imgSizeX, imgSizeY); //the moon.png in the middle
// \n means new line
let lines = '𝔥𝔢𝔩𝔩𝔬!\n𝔥𝔬𝔭𝔢 𝔶𝔬𝔲 𝔞𝔯𝔢 𝔡𝔬𝔦𝔫𝔤 𝔴𝔢𝔩𝔩\n𝔶𝔬𝔲 𝔰𝔥𝔬𝔲𝔩𝔡 𝔱𝔬𝔱𝔞𝔩𝔩𝔶 𝔭𝔯𝔢𝔰𝔰 𝔱𝔥𝔢\n𝔪𝔬𝔲𝔰𝔢 𝔟𝔲𝔱𝔱𝔬𝔫\n    𝔥𝔢𝔥𝔢 \n𝔟𝔲𝔱 𝔦𝔣 𝔶𝔬𝔲 𝔡𝔬𝔫𝔱 𝔴𝔞𝔫𝔱 𝔱𝔬\n𝔱𝔥𝔞𝔱 𝔦𝔰 𝔬𝔨𝔞𝔶 𝔱𝔬𝔬\n𝔶𝔬𝔲 𝔠𝔬𝔲𝔩𝔡 𝔞𝔩𝔰𝔬 𝔯𝔢𝔞𝔡\n𝔱𝔥𝔢 𝔯𝔦𝔤𝔥𝔱 𝔰𝔦𝔡𝔢 𝔬𝔣 𝔱𝔥𝔦𝔰 𝔭𝔞𝔤𝔢\n𝔦𝔣 𝔶𝔬𝔲 𝔣𝔢𝔢𝔩 𝔩𝔦𝔨𝔢 𝔦𝔱\n     𝔬k 𝔟𝔶𝔢\n x𝔬x𝔬 𝔑𝔦𝔫𝔞 \n (˵ ͡° ͜ʖ ͡°˵)';
fill(220, 220, 220);
textSize(20);

textLeading(50); //the spacing between lines
text(lines, 10, 35);

let lines1 = 'W𝔬𝔪𝔢𝔫 (+𝔱𝔯𝔞𝔫𝔰 +𝔫𝔟 𝔥𝔲𝔪𝔞𝔫𝔰)\n 𝔩𝔦𝔳𝔢 𝔴𝔦𝔱𝔥 𝔥𝔬𝔯𝔪𝔬𝔫𝔢𝔰\n 𝔱𝔥𝔞𝔱 𝔣𝔩𝔲𝔠𝔱𝔲𝔞𝔱𝔢 𝔤𝔯𝔢𝔞𝔱𝔩𝔶\n 𝔱𝔥𝔯𝔬𝔲𝔤𝔥𝔬𝔲𝔱 𝔞 𝟐𝟗 𝔡𝔞𝔶 𝔠𝔶𝔠𝔩𝔢\n 𝔧𝔲𝔰𝔱 𝔩𝔦𝔨𝔢 𝔱𝔥𝔢 𝔠𝔶𝔠𝔩𝔢 𝔬𝔣 𝔱𝔥𝔢\n                 𝔪𝔬𝔬𝔫 \n𝔱𝔥𝔬𝔲𝔤𝔥 𝔴𝔢 𝔞𝔯𝔢 𝔢𝔵𝔭𝔢𝔠𝔱𝔢𝔡 𝔱𝔬 𝔩𝔦𝔳𝔢\n 𝔞𝔰 𝔦𝔣 𝔴𝔢 𝔣𝔢𝔢𝔩 𝔱𝔥𝔢 𝔰𝔞𝔪𝔢 𝔢𝔳𝔢𝔯𝔶 𝔡𝔞𝔶\n ℑ 𝔣𝔦𝔫𝔡 𝔦𝔱 𝔢𝔫𝔱𝔦𝔯𝔢𝔩𝔶\n                 𝔭𝔞𝔱𝔯𝔦𝔞𝔯𝔠𝔥𝔞𝔩\n 𝔱𝔥𝔞𝔱 𝔴𝔢 𝔣𝔬𝔩𝔩𝔬𝔴 𝔱𝔥𝔢 𝔰𝔲𝔫𝔰 𝔠𝔶𝔠𝔩𝔢\n  𝔞𝔰 𝔬𝔭𝔭𝔬𝔰𝔢𝔡 𝔱𝔬\n                  𝔱𝔥𝔢 𝔪𝔬𝔬𝔫𝔰\n                (ﾉ´･ω･)ﾉ ﾐ ┸━┸';

fill(220, 220, 220);
textSize(20);

textLeading(50);
text(lines1, 1110, 35);

}

if (mouseIsPressed) {
  image (img1, 620, 240, img1sizeX, img1sizeY); //my earth.png
textSize(20);
  text ('(˵ ͡~ ͜ʖ ͡°˵)ﾉ⌒♡*:･。', 400, 30);

//thanks for reading xoxo Nina
}
}
