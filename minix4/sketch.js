let img1;
let img2;
let img1x;
let img1y;
let checkbox1;
let checkbox2;
let checkbox3;
let meme1;
let meme2;
let meme3;
let capture;
let positions;

function setup() {

frameRate(0.5);

createCanvas(1300, 900);

checkbox1 = createCheckbox('I consent', false);
  checkbox1.changed(myCheckedEvent1, true);
checkbox1.position(465, 600);

checkbox2 = createCheckbox('I consent?', false);
checkbox2.changed(myCheckedEvent2);
checkbox2.position(565, 600);

checkbox3 = createCheckbox('..I consent.. I guess', false);
checkbox3.changed(myCheckedEvent3);
checkbox3.position(665, 600);

let checked = true;
//button
button = createButton('I must rob you of your decision rights for my own economic growth');
button.style("color", "#ffff");
button.style("background", "#ffb8eb");
//  button.size(200,100);
button.position( 450, 20 );


img1 = loadImage ('letmecoloniseyou.png');
img1x = 350
img1y = 500


capture = createCapture(VIDEO);
capture.size(200, 300)
capture.hide();
//face ctracker
ctracker = new clm.tracker();
ctracker.init(pModel);
ctracker.start(capture.elt);

let positions  = ctracker.getCurrentPosition();


}

function draw() {

background(255, 220, 210, 30);

translate(0, 0);
fill(255);
rect(440, 80, 400, 550);
translate(0,0);
rect(390, 50, 500, 40);
textSize(20);
fill(0);
textFont("cursive");
text('(: FORM OF CONSENT :)', 530, 80);

meme1 = loadImage('sandata.jpg');
meme2 = loadImage('markhatesdata.jpg');
meme3 = loadImage('realhumanzuckerbergisreal.png');

image(img1, 460, 100, img1x, img1y);
background(255, 220, 210, 30);
}



function myCheckedEvent1() {
  if (this.checked()) {
    console.log('Checking!');
textSize(12);
fill(0);
text('*disclaimer!\n Before you engage be aware that in doing so\n you are contributing to the reinforcement of:\n the asymmetry of power\n the patriarchy\n inequality\n capitalism \n climate change \n and more ',50, 100);
  image(meme1, 700, 650, 300, 260);
  textLeading(20);


  }
}

function myCheckedEvent2() {
  if (this.checked()) {
    console.log('Checking!');
  image(meme2, 20, 240, 300, 270 );
  //memes here
} else {
  console.log('Unchecking!');

}
}

function myCheckedEvent3() {
  if (checkbox3.checked()) {

  image(meme3, 850, 100, 400, 360);
  //memes here
} else {
  console.log('Unchecking!');

}

}
