[**Let me colonise your data**](https://ninaisis.gitlab.io/aesthetic-programming-test/minix4)

<img src="minix4/Screenshot_2021-03-09_at_16.18.14.png" width="800">

**Transmediale submission:**
I feel too much pressure writing this part haha……..? (*0__0*)

**Description and process of the program**
So when you open this program your first thought might be “what?” first of all because your camera will turn on but you will not be able to see yourself…but also because you will see a consent form with weird religious references, this will all make better sense as you read further(I hope)

As I was very inspired by the surveillance capitalism text we read for class, I went searching for other things about it online, and here I found a video of a talk with Shoshanna Zuboff, in this she talked about how surveillance capitalism could be linked to colonialism and mentioned the anecdote of when the Spanish came to south America to colonize and wrote up an edict - this I will elaborate on later on in the read.me. Another thing that inspired me was this one quote from the required reading I really feel it relates well to my program; 

_”Datafication can be understood as itself a colonial process, not just in the metaphorical sense of saying things like “data is the new oil”, but quite literally as a new mode of data colonialism (Couldry and Mejias, 2019) that appropriates human life so that data can be continuously extracted from it for the benefit of particular (Western, but also increasingly global capitalist) interests. Instead of territories, natural resources, and enslaved labour, data colonialism appropriates social resources. While the modes, intensities, scales and contexts of data colonialism are different from those of historic colonialism, the function remains the same: to dispossess." (Mejias & Couldry, 2019, p 6.)”_

If you press the checkboxes on the edict/consent form a couple of memes will show up this is because I think that even though it is a serious issue I think many of us like coping with memes, but not only coping but also understanding things through humor, I know I do anyway. I personally feel like I learn a lot from sites like tiktok and Instagram that use this format to discuss serious topics but in an “easier” way. This paired with the colors and the smileys is also a way to “hide” the scariness this topic also holds. 


**Syntax:**
For this minix I struggled A LOT, I just couldn’t get my program to work the way I wanted it to, though I do think that in the end my program does convey all that I wanted to express, even if it is a little messy. I learnt and used a lot of new syntax this week; one is using DOM(), I used createCheckbox, I also got better at making my own functions and tried playing around with createCapture, though it is hidden, this was entirely on purpose as I like the fact that the webcam will turn on an show a little light but you can’t see what it sees. I also again played with the frameRate() and also played with the opacity of the background so the things that happen when this.checked slowly disappear into the background. This was at first a mistake, but I really like the idea of it hiding but not ever completely disappearing even if you remove your check, this is a reference to the fact that if you have given your data you can’t take it back, and you also can’t be sure where or which data is stored. I like making things a little meta haha. At last I have a button that doesn’t work, but It does have a cool quote and a cute color. 


**The theme of "capture all and cultural implications of data capture"**

Honestly, I have a lot of thoughts on this topic. I find Shoshanna Zuboffs claims to be very interesting. And in my program, I do touch upon many different subjects. The first thing you notice is a consent form, and if you read it, it probably won’t make a lot of sense, even though it is written in English, this consent form is actually a translation of the edict the Spanish made when they  colonized South America and wanted to do it “legally” to do this they wrote up an edict with the help of different priests and the pope and with this each time they came to a new area they would read out the edict in latin knowing full well that the indigenous population did not understand latin. After this they would proceed to steal the land, kill the people, and other horrifying things. 

Though it is not the same this does remind me of the “consent forms” we have to accept whenever we enter a new site or use a new program / platform, even though the forms are indeed written in English most people first of all do not want to read all the pages but even if one does I’m pretty sure when they write things like “let us access your data to better our product” it sounds innocent but in reality we have no idea what goes on when we accept, and who and where our data is stored/used. 

There is also the question of who is the “bad guy” is it the big tech companies? Technology? Mark Zuckerberg?(haha) of course these big companies have a responsibility, but in my opinion Capitalism might actually be the “bad guy”. A system that puts economic growth above human rights/the planet, is bound to wreak havoc. Colonialism and capitalism are also interconnected which is also a part of what my program encompasses. I like playing with these different themes all at once because firstly that is simply how my brain works, in my head everything is connected – but also because in my opinion many of the toxic traits of society today are a consequence of these things. 


xoxo Nina (also sorry this is so late)

references; 

http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf

https://theintercept.com/2019/03/01/surveillance-capitalism-book-shoshana-zuboff-naomi-klein/

https://kdhist.sitehost.iu.edu/H105-documents-web/week02/Requerimiento1513.html

The Costs of Connection: How Data Is Colonizing Human Life and Appropriating It for Capitalism
AU  - Mejias, Ulises A.

all memes were found on google images :) 
