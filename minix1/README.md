# Aesthetic Programming minix 1 by Nina Isis

[link to 'The Orbit of Shapes'](https://ninaisis.gitlab.io/aesthetic-programming-test/minix1/)
<img src="Screenshot_2021-02-07_at_21.47.45.png" width="600">


**what have I produced?**
For this weeks minix(1) I have made a little moving pastel planet with shapes orbiting around it. I've named it **The Orbit of Shapes** :-)
as this is the first time coding, I just started playing around with shapes and colours, trying to figure out how to place objects, and colour them. I then had the idea of wanting to make something spacey, like creating different planets and such, though this proved to be too hard for me to do so I just thought I would use the shapes and colours I had already played around with and made a big sphere in den middle. I then thought 'this is too basic' and tried to find other syntax that could spice it up a little, I here found the rotating syntax, and tried playing around with that at last I added the command that if one presses the left mouse button the shapes change direction and speed.- and that was how **The Orbit of Shapes** was made. There were many other things I tried to add, like changing the cursor, or the action that made it rotate, inserting text or a button but this was all too hard so i kept it as simple as possible while still pushing myself a little. 

**How would you describe your first independent coding experience (in relation to
thinking, reading, copying, modifying, writing code, and so on)?**

My first coding experience was first of all frustrating but also very fun, it's amazing when something just suddenly makes sense. One big thing I found out was that I learned a lot more when starting small and gradually trying new things, with a focus on learning and not necessarily making the coolest most complex thing that I don't understand.

**How is the coding process different from, or similar to, reading and writing text?**

It is similar in the way it has to be written in a specific order for it to make sense, both for the person coding but also for the computer. just like grammar and punctuations matter in writing, so does putting in the right commands, and all that matter in when coding. Though a difference is when writing or reading non-code the text has to be very messed up for it to not make sense whereas one small mistake in code will make it unreadable.

references:

http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf
https://p5js.org/reference/#/p5/sphere


**What does code and programming mean to you, and how does the assigned
reading help you to further reflect on these terms?** 

Programming is an exciting and new way to be creative. I'm excited to be able to express myself better within this medium as i learn more and more. I'm also excited to learn more from reading, and I hope to become more aware of the possibilities and also the consequences within the realm of coding.

