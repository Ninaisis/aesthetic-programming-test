let x = 200;
let y = 200;
let extraCanvas;
let myfont;
let _text;



function setup() {
  // put setup code here
_text = createGraphics(window.innerWidth - 4, window.innerHeight -4); //my other canvas made for the text

_text.textFont('Source Code Pro');
 _text.textAlign('rect');
 _text.textSize(40);
 _text.fill(3, 7, 11);
 _text.stroke(255);
 _text.text('♥︎ what if all the software\n we used was made by people\n who love us? ♥︎\n -Melanie Hoff', 100, 80);


  extraCanvas = createGraphics(1600, 800, P2D); //my canvas for the text box
  extraCanvas.clear();
  createCanvas(1400, 800, WEBGL); //my main canvas where all the fun happens
background(0);
frameRate(7); //played with this

fill('pink');

}


function draw() {
  //background(0);




push();

let time = millis();
  rotateX(time / 1000);
  rotateZ(time - 5);

  fill(0);
  sphere(200, 40, 100);
fill('pink', );
sphere(200, 40);

//all my damn shapes
fill(200,220,255,10);
stroke('pink')
ellipse(170+10, 250+10, 140, 99);

fill(230,200,240,10);
rect(220+100, 100+10,100, 80);


fill(235, 255, 245,10);
rect(270+20, -30-10, 50, 100)

fill(250, 255, 230,10);
ellipse(275+10, -100-10, 100, 100);

fill(255, 224, 210,10);
rect(220+40, -280+50, 20, 100);

fill(255, 190,255,10);
square(130+10,-300-100,55,20);

fill(255,255,200,10);
rect(10+100, -320+10, 100, 60);

fill(200, 245, 200,10);
ellipse(-40-40, -290+20, 60, 110);

fill(255, 200, 230,10);
rect(-145+100, -310-300, 60, 100);

fill(255,170,255,10);
ellipse(-220, -230, 100, 50);

fill(255, 190, 255,10);
rect(-270, -200, 30, 140);

fill(255,230, 190,10);
ellipse(-280, -0, 100, 100);

fill(200, 200, 255,10);
square(-310, 90, 100, 60);

fill(200, 190, 255,10);
ellipse(-150, 250, 80, 120);

fill(200, 220,220,20);
rect(-60, 250, 120, 60);


pop();
//all the things that shouldn't rotate/move
noStroke();
  texture(_text); //my text
plane(window.innerWidth - 4, window.innerHeight - 4);
image(extraCanvas, 0, 0); //my text box
  fill(20,20,20,0);
  stroke('pink');
rect(-620, -380, 500, 300);

}
