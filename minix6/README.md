[*softer cyberspace*](https://ninaisis.gitlab.io/aesthetic-programming-test/minix6)


<img src="minix6/Screenshot_2021-04-03_at_15.31.52.png" width="800">




**What does aesthetic programming mean (to me)?**

This is what I wrote when asked this specific question when making my first minix _>Programming is an exciting and new way to be creative. I'm excited to be able to express myself better within this medium as i learn more and more. I'm also excited to learn more from reading, and I hope to become more aware of the possibilities and also the consequences within the realm of coding.>_ to follow this I would definitely say that all the things i had hoped to learn through aesthetic programming so far, have come true. I have also found out not only how to consciously consider the possibilities and consequences programming can have, but also been made aware that it is possible to look at the sociotechnological effects through programming itself. Especially the critical theory / critical design aspect has really caught my attention, and is definitely something I could see myself further exploring in the future. 

**Description of the program and its changes**

So for this weeks minix6 we were tasked with looking at one of our old minix and changing it - I chose to change my first program as I thought it needed a stronger foundation conceptually. My minix1 was my first time programming, and for me it was all about getting familiar with atom. I mainly focused on learning to make shapes in different sizes and placing them on the canvas. I also played around with colours a lot, this was before i figured out you could get specific RGB numbers online... When I chose this minix er was very sure that I conceptually hadn't thought of anything and it was going to be my mission to figure out what i felt it could express. But upon further inspection and reflection I thought to myself "even though I didn't consciously have any thing I wanted to express when making it it will still have expression embedded within" So I thought it's true what I made has a very feminine feel, and also resembles some sort of planet, this made me think of something I have been thinking about a lot recently, which is the notion of making software/cyberspace "softer" how can we make it more feminine feeling, inclusive, intersectional, and inviting? which then made me think about this really great quote by Melanie Hoff (an artist/programmer) that says "what if all the software we used was made by people who love us?" which I think is the perfect quote to encompass the meaning of softer software. 


<img src="minix6/Screenshot_2021-02-07_at_21.47.45.png" width="400"> <img src="minix6/Screenshot_2021-04-03_at_15.31.52.png" width="430">

 So with this in mind I started playing around with the code, even though i liked the pastel colours I wanted the colours to contrast a little more so i made the background black and made all the stroke() pink, and also turned down the opacity on the shapes so that only the line was clear. I also wanted to keep the rotation and also the sphere which made it look like a planet to be an expression of "cyberspace". I also really wanted to add the quote by Melanie Hoff, but this turned out to be the hardest task as I had made my program via WEBGL which meant I couldnt use text/fonts the same way as I had before. This is when I remembered having learnt how to make multiple canvases, so I made a new canvas and made it clear. This fixed it. I also wanted to add some generative aspect so I made some of the shapes move on the x and y axes after each frame, which again made it feel more "spacey". I also used the push() pop() function to make sure only the shapes and sphere were rotating. So in the end my new program is just an upgraded version with a little more of a clearer vision in mind. 


**Programming as a practice/method for design and its relation to digital culture**

In some ways i feel that after learning more and more about programming it is just as elusive a concept as "art" and "design" are. The possibilities are endless, and it's way more pliable than I had imagined, which is also what makes it such an interesting thing to learn about. It is in my opinion a very crucial part of "design" especially these days. The more I learn the more I understand the possibilities but also the consequences programming can have. I really like how programming can intersect with design and art and how this creates many new ways to express and understand digital culturem but also how it shapes it, and how this if unchecked can have major consequences. 


xoxo Nina (better late than never right :( :) )


